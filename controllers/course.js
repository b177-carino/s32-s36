const Course = require("../models/Course");

// Controller function for creating a course
module.exports.addCourse = (data) => {

	// User is an admin
	if (data.isAdmin) {

		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});

		// Saves the created object to our database
		return newCourse.save().then((course, error) => {

			// Course creation failed
			if (error) {

				return false;

			// Course creation successful
			} else {

				return true;

			};

		});

	// User is not an admin
	} else {
		return Promise.resolve(false);
	};
	

};

// Controller function for getting all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

// Controller function for retrieving a specific course 
module.exports.getCourse = (reqParams) => {
    return Course.findById(reqParams.courseId).then(result => {
        return result; 
    })
}

// Controller function for updating a specific course
module.exports.updateCourse = (reqParams, reqBody) => {
    // Specify the fields/properties of the document to be updated
    let updatedCourse = {
        name : reqBody.name, 
        description : reqBody.description,
        price : reqBody.price
    }

    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
        if(error){
            return false;
        }
        else {
            return true;
        }
    })
}

// [Activity] Controller function for archiving a course
module.exports.archiveCourse = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {

		// Course not archived
		if (error) {

			return false;

		// Course archived successfully
		} else {

			return true;

		}

	});
};


